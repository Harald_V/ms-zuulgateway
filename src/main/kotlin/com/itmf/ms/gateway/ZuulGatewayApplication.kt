package com.itmf.ms.gateway

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.netflix.zuul.EnableZuulProxy

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
class ZuulGatewayApplication

fun main(args: Array<String>) {
	runApplication<ZuulGatewayApplication>(*args)
}
