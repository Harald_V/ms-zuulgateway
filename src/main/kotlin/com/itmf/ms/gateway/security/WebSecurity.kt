package com.itmf.ms.gateway.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy

@Configuration
@EnableWebSecurity
class WebSecurity : WebSecurityConfigurerAdapter() {

	lateinit var env: Environment

	@Autowired
	fun WebSecurity() {
		env = this.getApplicationContext().getBean(Environment::class.java)
	}

	override
	fun configure(http: HttpSecurity) {
		val register = env.getProperty("api.url.path.registration") // /users
		val login = env.getProperty("api.url.path.login") // /users/login"
		val status = env.getProperty("api.url.path.status") // /users/status/check
		val console = env.getProperty("api.h2console.url.path.common") // /h2/**

		val actuator = env.getProperty("api.zuul.url.path.actuator")
		val userActuator = env.getProperty("api.user.url.path.actuator")

		val authFilter = AuthorisationFilter(authenticationManager(), env)

		http.csrf().disable()
		http.headers().frameOptions().disable()

		http.authorizeRequests()
			.antMatchers(console).permitAll()
			.antMatchers(status).permitAll()
			.antMatchers(actuator).permitAll()
			.antMatchers(userActuator).permitAll()

			.antMatchers(HttpMethod.POST, register).permitAll()
			.antMatchers(HttpMethod.POST, login).permitAll()

			.anyRequest().authenticated()
			.and().addFilter(authFilter)

		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	}

}