package com.itmf.ms.gateway.security

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.io.Decoders
import io.jsonwebtoken.security.Keys
import org.springframework.core.env.Environment
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import java.security.Key
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class AuthorisationFilter(authManager: AuthenticationManager, var env: Environment) : BasicAuthenticationFilter(authManager) {

	//always called !!
	override
	fun doFilterInternal(req: HttpServletRequest, res: HttpServletResponse, filterChain: FilterChain) {

		val headerName = env.getProperty("authorization.token.header.name")
		val prefix = env.getProperty("authorization.token.header.prefix")

		val authorHeader = req.getHeader(headerName)

		if(authorHeader == null || authorHeader.startsWith(prefix.toString()) == false) { //
			filterChain.doFilter(req, res)
			return
		}

		val usernamepwdVerif = getAuthentication(authorHeader)

		SecurityContextHolder.getContext().authentication = usernamepwdVerif
		filterChain.doFilter(req, res)
	}

	private fun getAuthentication(authorHeader: String): UsernamePasswordAuthenticationToken? {

		val token: String = authorHeader.replace(env.getProperty("authorization.token.header.prefix").toString(), "")

		var userID: String? = null
		try {

			val tokensecret = env.getProperty("token.secret")
			val keyBytes: ByteArray = Decoders.BASE64.decode(tokensecret)
			val key: Key = Keys.hmacShaKeyFor(keyBytes)

			userID = Jwts.parserBuilder()
				.setSigningKey(key)
				.build().parseClaimsJws(token).body.subject
		} catch(e: Exception) {
			e.printStackTrace()
			userID = null
		}

		if(userID == null) {
			return UsernamePasswordAuthenticationToken(null, null )//default, isAuthenticated==false
		}
		//it's ok, we can set principal and auth the request, //default, isAuthenticated==true
		return UsernamePasswordAuthenticationToken(userID, null , ArrayList())
	}
}


